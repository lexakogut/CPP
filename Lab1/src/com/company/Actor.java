package com.company;

public class Actor extends Employee {
    public Actor(String firstname, String lastname) {
        super(firstname, lastname);
    }

    @Override
    public String toString() {
        return String.format("[Actor] %s %s", firstname, lastname);
    }
}
