package com.company;

public class Employee {

    protected String firstname;
    protected String lastname;

    public String getFirstName() {
        return firstname;
    }
    public String getLastName() {
        return lastname;
    }

    public Employee(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return String.format("%s %s", firstname, lastname);
    }
}
