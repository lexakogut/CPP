package com.company;

import java.io.InputStream;
import java.util.Scanner;
import java.util.zip.DataFormatException;

public class EmployeeFileReader {
    public Employee[] Read(InputStream stream) throws DataFormatException {
        Scanner scanner = new Scanner(stream);

        if (!scanner.hasNextInt()) throw new DataFormatException("No employee count");
        int count = scanner.nextInt();
        scanner.nextLine();

        Employee[] employees = new Employee[count];

        for (int i = 0; i < count; ++i)
        {
            if (!scanner.hasNextLine()) throw new DataFormatException("Employee count is less than expected");
            String type = scanner.next("\\w+");
            String firstname = scanner.next("\\w+");
            String lastname = scanner.next("\\w+");

            switch (type)
            {
                case "PRODUCER":
                    employees[i] = new Employee(firstname, lastname);
                    break;
                case "OPERATOR":
                    String equipment = scanner.next("\\w+");
                    employees[i] = new Operator(firstname, lastname, equipment);
                    break;
                case "ACTOR":
                    employees[i] = new Actor(firstname, lastname);
                    break;
            }
            scanner.nextLine();
        }

        if (scanner.hasNextLine()) throw new DataFormatException("Employee count is more than expected");

        return employees;
    }
}
