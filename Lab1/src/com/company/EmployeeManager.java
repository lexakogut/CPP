package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

public class EmployeeManager {
    static class FirstnameComparator implements Comparator<Employee> {
        public boolean isAscending;

        @Override
        public int compare(Employee o1, Employee o2) {
            int result = o1.getFirstName().compareToIgnoreCase(o2.getFirstName());
            return isAscending ? result : -result;
        }

        public FirstnameComparator(boolean isAscending) {
            this.isAscending = isAscending;
        }
    }

    Comparator<Employee> LastnameAscendingComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            int result = o1.getLastName().compareToIgnoreCase(o2.getLastName());
            return result;
        }
    };

    Comparator<Employee> LastnameDescendingComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            int result = o1.getLastName().compareToIgnoreCase(o2.getLastName());
            return -result;
        }
    };

    public Collection<Employee> FindEmployees(ArrayList<Employee> employees, String query)
    {
        ArrayList<Employee> result = new ArrayList<Employee>();
        employees.forEach ((employee) ->  {
            if (employee.getFirstName().contains(query) || employee.getLastName().contains(query))
            {
                result.add(employee);
            }
        });
        return result;
    }

    public void SortByFirstName(ArrayList<Employee> employees, boolean isAscending)
    {
        employees.sort(new FirstnameComparator(isAscending));
    }

    public void SortByLastName(ArrayList<Employee> employees, boolean isAscending)
    {
        employees.sort(isAscending ? LastnameAscendingComparator : LastnameDescendingComparator);
    }
}
