package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Scanner;
import java.util.zip.DataFormatException;

/*
Кіностудія. Реалізувати ієрархію посад, які залучені до виробництва кінофільму.
Реалізувати пошук працівників, які необхідні для зйомки документального фільму.
Реалізувати можливість сортування знайдених посад за двома типами параметрів
(на вибір, реалізовано як два окремі методи)
Реалізація сортування має передбачати можливість сортувати як за спаданням, так і за зростанням
 */

class MenuContext
{
    public EmployeeManager manager;
    public ArrayList<Employee> employees;
    public ArrayList<Movie> movies;
    public Scanner scanner;

    public MenuContext(Employee[] employees, Movie[] movies) {
        this.employees = new ArrayList<>(Arrays.asList(employees));
        this.movies = new ArrayList<>(Arrays.asList(movies));
        manager = new EmployeeManager();
        scanner = new Scanner(System.in);
    }
}

public class Main {
    static void PrintEmployees(MenuContext context)
    {
        for (Employee employee : context.employees)
        {
            System.out.println(employee.toString());
        }
    }

    static void PrintMovies(MenuContext context)
    {
        int i = 1;
        for (Movie movie : context.movies)
        {
            System.out.printf("[%d] %s %n", i, movie.toString());
            ++i;
        }
    }

    static void PrintMovie(Movie movie)
    {
        System.out.printf("%s %nEmployees involved: %n", movie.toString());
        for (Employee employee : movie.employeesInvolved) {
            System.out.printf(" - %s%n", employee.toString());
        }
    }

    static void Help()
    {
        System.out.printf("0. Help; %n");
        System.out.printf("1. Print all employees; %n");
        System.out.printf("2. Sort by first name (ascending order); %n");
        System.out.printf("3. Sort by first name (descending order); %n");
        System.out.printf("4. Sort by last name (ascending order);  %n");
        System.out.printf("5. Sort by last name (descending order);  %n");
        System.out.printf("6. Find employees;  %n");
        System.out.printf("7. Print all movies;  %n");
        System.out.printf("8. Print detailed information about a movie;  %n");
        System.out.printf("9. Exit;  %n");
    }

    static void SelectionMenu(MenuContext context)
    {
        while (true) {
            System.out.printf("Select your option:%n");

            while (true) {
                if (!context.scanner.hasNextInt()) {
                    System.out.printf("what? try writing 0 you idiot %n");
                    context.scanner.nextLine();
                } else break;
            }
            int choice = context.scanner.nextInt();
            switch (choice) {
                case 0:
                    Help();
                    break;
                case 1:
                    PrintEmployees(context);
                    break;
                case 2:
                    context.manager.SortByFirstName(context.employees, true);
                    System.out.printf("Sorted by first name (ascending) %n");
                    break;
                case 3:
                    context.manager.SortByFirstName(context.employees, false);
                    System.out.printf("Sorted by first name (descending) %n");
                    break;
                case 4:
                    context.manager.SortByLastName(context.employees, true);
                    System.out.printf("Sorted by last name (ascending) %n");
                    break;
                case 5:
                    context.manager.SortByLastName(context.employees, false);
                    System.out.printf("Sorted by last name (descending) %n");
                    break;
                case 6:
                    System.out.printf("Enter your query: %n");
                    String query = context.scanner.next("\\w+");
                    Collection<Employee> foundEmployees = context.manager.FindEmployees(context.employees, query);
                    foundEmployees.forEach((employee -> {
                        System.out.println(employee.toString()); }));
                    break;
                case 7:
                    PrintMovies(context);
                    break;
                case 8:
                    System.out.printf("Index of a movie? %n");
                    int index = context.scanner.nextInt() - 1;
                    if (index < 0 || index > context.movies.size())
                    {
                        System.out.printf("Incorrect index! %n");
                        break;
                    }
                    Movie movie = context.movies.get(index);
                    PrintMovie(movie);
                    break;
                case 9:
                    System.out.printf("Goodbye %n");
                    return;
            }
        }
    }

    public static void main(String[] args) {
        try {
            InputStream employeeStream = new FileInputStream(new File("employees.txt"));
            InputStream movieStream = new FileInputStream(new File("movies.txt"));
            EmployeeFileReader employeeFileReader = new EmployeeFileReader();
            MovieFileReader movieFileReader = new MovieFileReader();

            Employee[] employees = employeeFileReader.Read(employeeStream);
            Movie[] movies = movieFileReader.Read(movieStream, employees);
            employeeStream.close();
            movieStream.close();
            MenuContext context = new MenuContext(employees, movies);
            SelectionMenu(context);
        } catch (IOException ex)
        {
            System.out.printf("Error: io problem (%s)%n", ex.getMessage());
        } catch (DataFormatException ex)
        {
            System.out.printf("Error: invalid file format (%s)%n", ex.getMessage());
        }
    }
}
