package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Movie {
    protected ArrayList<Employee> employeesInvolved;

    protected String name;
    protected String genre;

    public String getName()
    {
        return name;
    }
    public String getGenre()
    {
        return genre;
    }

    public Movie(String name, String genre, Employee[] employeesInvolved)
    {
        this.name = name;
        this.genre = genre;
        this.employeesInvolved = new ArrayList<Employee>(Arrays.asList(employeesInvolved));
    }

    @Override
    public String toString() {
        return String.format("\"%s\" (%s)", name, genre);
    }
}
