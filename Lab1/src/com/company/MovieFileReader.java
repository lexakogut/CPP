package com.company;

import java.io.InputStream;
import java.util.Scanner;
import java.util.zip.DataFormatException;

public class MovieFileReader {
    public Movie[] Read(InputStream stream, Employee[] employees) throws DataFormatException {
        Scanner scanner = new Scanner(stream);

        if (!scanner.hasNextInt()) throw new DataFormatException("No movie count");
        int count = scanner.nextInt();
        scanner.nextLine();

        Movie[] movies = new Movie[count];

        for (int i = 0; i < count; ++i)
        {
            if (!scanner.hasNextLine()) throw new DataFormatException("Employee count is less than expected");
            String name = scanner.next("\\w+");
            String genre = scanner.next("\\w+");
            int employeeCount = scanner.nextInt();

            Employee[] employeesInvolved = new Employee[employeeCount];
            for (int e = 0; e < employeeCount; ++e)
            {
                int employeeIndex = scanner.nextInt();
                employeesInvolved[e] = employees[employeeIndex];
            }
            movies[i] = new Movie(name, genre, employeesInvolved);
            scanner.nextLine();
        }

        if (scanner.hasNextLine()) throw new DataFormatException("Employee count is more than expected");

        return movies;
    }
}
