package com.company;

public class Operator extends Employee {

    String equipment;

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public Operator(String firstname, String lastname, String equipment) {
        super(firstname, lastname);
        this.equipment = equipment;
    }

    @Override
    public String toString() {
        return String.format("[Operator] %s %s equipped with: %s", firstname, lastname, equipment);
    }
}
