package com.company;

public class Producer extends Employee {
    public Producer(String firstname, String lastname) {
        super(firstname, lastname);
    }

    @Override
    public String toString() {
        return String.format("[Producer] %s %s", firstname, lastname);
    }
}
