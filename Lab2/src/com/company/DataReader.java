package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class DataReader {
    private Scanner getScanner(String path) throws FileNotFoundException
    {
        return new Scanner(new FileInputStream(path));
    }

    public ArrayList<Student> readStudents(String path) throws FileNotFoundException
    {
        Scanner scanner = getScanner(path);
        ArrayList<Student> result = new ArrayList<>();

        while (scanner.hasNextLine())
        {
            String firstname = scanner.next("\\w+");
            String lastname = scanner.next("\\w+");
            int courseCount = scanner.nextInt();

            ArrayList<Course> courses = new ArrayList<>();
            for (int e = 0; e < courseCount; ++e)
            {
                String courseName = scanner.next("\\w+");
                courses.add(new Course(courseName));
            }
            result.add(new Student(firstname, lastname, courses));
            scanner.nextLine();
        }

        return result;
    }

    public ArrayList<Teacher> readTeachers(String path) throws FileNotFoundException
    {
        Scanner scanner = getScanner(path);
        ArrayList<Teacher> result = new ArrayList<>();

        while (scanner.hasNextLine())
        {
            String firstname = scanner.next("\\w+");
            String lastname = scanner.next("\\w+");
            int courseCount = scanner.nextInt();

            ArrayList<Course> courses = new ArrayList<>();
            for (int e = 0; e < courseCount; ++e)
            {
                String courseName = scanner.next("\\w+");
                courses.add(new Course(courseName));
            }
            result.add(new Teacher(firstname, lastname, courses));
            scanner.nextLine();
        }

        return result;
    }
}
