package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class MenuContext
{
    public DataReader reader;
    public ArrayList<Student> students;
    public ArrayList<Teacher> teachers1;
    public ArrayList<Teacher> teachers2;
    public Scanner scanner;

    public MenuContext(String studentsPath, String teachers1Path,  String teachers2Path)
            throws FileNotFoundException
    {
        this.reader = new DataReader();
        scanner = new Scanner(System.in);
        this.students = reader.readStudents("students.txt");
        this.teachers1 = reader.readTeachers("teachers1.txt");
        this.teachers2 = reader.readTeachers("teachers2.txt");
    }
}

public class Main {
    private static void help(MenuContext context)
    {
        System.out.println("0. This command;");
        System.out.println("1. Print students;");
        System.out.println("2. Print common students of 2 courses;");
        System.out.println("3. Print top teachers by course count;");
        System.out.println("4. Exit;");
    }

    private static String coursesToString(ArrayList<Course> courses)
    {
        StringBuilder coursesStringBuilder = new StringBuilder();
        for (Course course : courses)
        {
            coursesStringBuilder.append(course.name);
            coursesStringBuilder.append(", ");
        }
        return coursesStringBuilder.substring(0, coursesStringBuilder.length() - 2);
    }

    private static void printStudentTable(MenuContext context)
    {
        System.out.printf("| %-15s | %-15s | %-50s |%n", "[Firstname]", "[Lastname]", "[Courses]");
        for (Student student : context.students)
        {
            System.out.printf("| %-15s | %-15s | %-50s |%n",
                    student.firstname, student.lastname, coursesToString(student.coursesLearning));
        }
        System.out.printf("%n");
    }

    private static void commonCourseMenu(MenuContext context)
    {
        System.out.print("Enter course 1 please: ");
        Course course1 = new Course(context.scanner.next("\\w+"));
        System.out.print("Enter course 2 please: ");
        Course course2 = new Course(context.scanner.next("\\w+"));

        ArrayList<Student> studentsFromCourse1 = new ArrayList<>();
        ArrayList<Student> studentsFromCourse2 = new ArrayList<>();

        for (Student student : context.students)
        {
            for (Course course : student.coursesLearning)
            {
                if (course.name.equalsIgnoreCase(course1.name)) studentsFromCourse1.add(student);
                if (course.name.equalsIgnoreCase(course2.name)) studentsFromCourse2.add(student);
            }
        }

        ArrayList<Student> studentsFromBothCourses = new ArrayList<>(studentsFromCourse1);

        studentsFromBothCourses.retainAll(studentsFromCourse2);

        if (studentsFromBothCourses.size() == 0)
        {
            System.out.printf("No common students found :(");
        }
        else for (Student student : studentsFromBothCourses)
        {
            System.out.printf("%s %s, courses: %s%n",
                    student.firstname, student.lastname, coursesToString(student.coursesLearning));
        }
    }

    private static void topTeachersMenu(MenuContext context) {
        ArrayList<Teacher> allTeachers = new ArrayList<>();
        allTeachers.addAll(context.teachers1);
        allTeachers.addAll(context.teachers2);

        int totalCourses = 0;
        for (Teacher teacher : allTeachers)
        {
            totalCourses += teacher.coursesTaught.size();
        }
        float avgCourses = (float)totalCourses / allTeachers.size();

        ArrayList<Teacher> teachersWithBiggerThanAvgCourseCount = new ArrayList<>();
        for (Teacher teacher : allTeachers)
        {
            if (teacher.coursesTaught.size() > avgCourses)
                teachersWithBiggerThanAvgCourseCount.add(teacher);
        }

        System.out.printf("Average courses: %f%n", avgCourses);
        for (Teacher teacher : teachersWithBiggerThanAvgCourseCount)
        {
            System.out.printf("%s %s, courses: %s%n",
                    teacher.firstname, teacher.lastname, coursesToString(teacher.coursesTaught));
        }
    }

    public static void main(String[] args)
    {
        MenuContext context;
        try
        {
            context = new MenuContext("students.txt", "teachers1.txt", "teachers2.txt");
        }
        catch (FileNotFoundException e)
        {
            System.out.println("loh e file not found");
            return;
        }

        while (true) {
            System.out.printf("Select your option:%n");

            while (true) {
                if (!context.scanner.hasNextInt()) {
                    System.out.printf("what? try writing 0 you idiot %n");
                    context.scanner.nextLine();
                } else break;
            }
            int choice = context.scanner.nextInt();
            switch (choice) {
                case 0:
                    help(context);
                    break;
                case 1:
                    printStudentTable(context);
                    break;
                case 2:
                    commonCourseMenu(context);
                    break;
                case 3:
                    topTeachersMenu(context);
                    break;
                case 4:
                    System.out.println("bye");
                    return;
                default:
                    System.out.println("you dumbo, type 0");
                    break;
            }
        }
    }
}
