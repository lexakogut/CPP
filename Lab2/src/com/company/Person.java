package com.company;

public class Person {
    public final String firstname, lastname;

    public Person(String firstname, String lastname)
    {
        this.firstname = firstname;
        this.lastname = lastname;
    }
}
