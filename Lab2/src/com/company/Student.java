package com.company;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Student extends Person {
    public ArrayList<Course> coursesLearning;

    public Student(String firstname, String lastname, Collection<Course> coursesLearning) {
        super(firstname, lastname);
        this.coursesLearning = new ArrayList<>(coursesLearning);
    }

    public Student(String firstname, String lastname) {
        this(firstname, lastname, new ArrayList<>());
    }

    public void SubscribeToCourse(Course course)
    {
        this.coursesLearning.add(course);
    }
}
