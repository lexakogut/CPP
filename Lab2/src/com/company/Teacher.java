package com.company;

import java.util.ArrayList;

public class Teacher extends Person {
    public ArrayList<Course> coursesTaught;

    public Teacher(String firstname, String lastname, ArrayList<Course> coursesTaught) {
        super(firstname, lastname);
        this.coursesTaught = new ArrayList<>(coursesTaught);
    }
}
