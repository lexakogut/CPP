package com.company;

import jdk.nashorn.internal.parser.DateParser;

import javax.swing.text.DateFormatter;
import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.zip.DataFormatException;

public class Main {
    private static final DateFormat defaultFormat = new SimpleDateFormat("dd.MM.yyyy");
    private static final DateFormat formatWithWeekDay = new SimpleDateFormat("dd.MM.yyyy (EEE)", Locale.ROOT);

    private static final DateFormat[] formats = new DateFormat[]
            {
                    new SimpleDateFormat("MM/dd/yy"),
                    new SimpleDateFormat("yyyy-MM-dd"),
                    new SimpleDateFormat("d MMM yyyy", Locale.ROOT),
                    new SimpleDateFormat("MMMM d, yyyy", Locale.ROOT),
                    new SimpleDateFormat("yyyyMMdd"),
                    defaultFormat,
                    formatWithWeekDay
            };

    private static Date tryParseDate(String text) throws DataFormatException
    {
        for (DateFormat format : formats) {
            try
            {
                return format.parse(text);
            }
            catch (ParseException e) {  }
        }
        throw new DataFormatException("Unknown format");
    }

    private static String readAllTextFromFile(String path) throws FileNotFoundException, IOException
    {
        FileInputStream stream = new FileInputStream(path);
        InputStreamReader reader = new InputStreamReader(stream);
        StringBuilder builder = new StringBuilder();
        int readResult = 0;
        while ((readResult = reader.read()) != -1) {
            char symbol = (char)readResult;
            builder.append(symbol);
        }
        return builder.toString();
    }

    private static List<Date> parseDatesFromText(String text)
    {
        ArrayList<Date> dates = new ArrayList<>();
        Scanner scanner = new Scanner(text);
        String[] last3TokenParts = new String[] { "", "", "" };
        while(scanner.hasNext())
        {
            // shift everyting back
            for (int i = 1; i < last3TokenParts.length; ++i)
            {
                last3TokenParts[i - 1] = last3TokenParts[i];
            }
            String lastToken = scanner.next();
            try {
                Date dateParsed = tryParseDate(lastToken);
                dates.add(dateParsed);
                System.out.printf("\"%s\" is a date!%n", lastToken);
            }
            catch (Exception e1)
            {
                last3TokenParts[last3TokenParts.length - 1] = lastToken;
                System.out.printf("\"%s\" is not a date%n", lastToken);
                StringBuilder builder = new StringBuilder();
                for(String part : last3TokenParts) {
                    builder.append(part);
                    builder.append(' ');
                }
                String last3Tokens = builder.toString();
                try {
                    Date dateParsed = tryParseDate(last3Tokens);
                    dates.add(dateParsed);
                    System.out.printf("\"%s\" is a date!%n", last3Tokens);
                }
                catch (Exception e2)
                {
                    System.out.printf("\"%s\" is not a date either%n", last3Tokens);
                }
            }
        }
        return dates;
    }

    public static void main(String[] args)
    {
        String text;
        try {
            text = readAllTextFromFile("data.txt");
            System.out.println(text);
        }
        catch (Exception e) {
            System.out.printf("loh e take this exception: %s%n", e.toString());
            return;
        }

        List<Date> parsedDates;
        try {
            parsedDates = parseDatesFromText(text);
        }
        catch (Exception e) {
            System.out.printf("loh e take this exception: %s%n", e.toString());
            return;
        }

        System.out.printf("%n-----------------RESULT-----------------%n");

        Date minDate = new Date(Long.MAX_VALUE);
        Date maxDate = new Date(Long.MIN_VALUE);

        System.out.printf("Found %d dates.%n", parsedDates.size());
        for (Date date : parsedDates)
        {
            System.out.println(defaultFormat.format(date));

            if (date.compareTo(minDate) < 0) minDate = date;
            if (date.compareTo(maxDate) > 0) maxDate = date;
        }

        System.out.printf("Range of dates: %s - %s%n", defaultFormat.format(minDate), defaultFormat.format(maxDate));

        System.out.println("Adding one day to every date...");
        for (Date date : parsedDates)
        {
            final long millisInDay = 24 * 60 * 60 * 1000;
            date.setTime(date.getTime() + millisInDay);
            System.out.println(formatWithWeekDay.format(date));
        }
    }
}
