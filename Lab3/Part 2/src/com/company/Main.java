package com.company;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Scanner;
import java.util.zip.DataFormatException;

public class Main {
    private static String readAllText(String path) throws IOException {
        StringBuilder sb = new StringBuilder(512);
        Reader r = new InputStreamReader(new FileInputStream(path), "UTF-8");
        int c = 0;
        while ((c = r.read()) != -1) {
            sb.append((char) c);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TextProcessor processor = new TextProcessor(true);
        String loremIpsum = null;
        try {
            loremIpsum = readAllText("loremIpsum.txt");
        }
        catch (IOException e) {
            System.out.println("lorem ipsum died :(");
        }

        System.out.println("Write your text below or just press ENTER to use lorem ipsum text.");
        String userText = scanner.nextLine();
        if (userText.isEmpty())
        {
            System.out.println("[no text entered, using lorem ipsum]");

            userText = loremIpsum;
        }
        try {
            StringBuffer buffer = new StringBuffer(userText);
            System.out.println("Text before processing:");
            System.out.println(buffer);
            processor.processText(buffer);

            System.out.println("Text after processing:");
            System.out.println(buffer);
        } catch (DataFormatException e) {
            System.out.println("loh e");
        }
    }
}
