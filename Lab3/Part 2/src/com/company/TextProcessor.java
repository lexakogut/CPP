package com.company;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;

public class TextProcessor {
    public boolean debugMode;

    public TextProcessor(boolean debugMode)
    {
        this.debugMode = debugMode;
    }

    public void processSentence(StringBuffer text, int startIndex, int endIndex) {
        String sentenceBefore = text.substring(startIndex, endIndex);

        Pattern anyWordPattern = Pattern.compile("\\w+\\b");
        Pattern vowelPattern = Pattern.compile("\\b[aAoOeEiIuUyY]\\w+\\b");

        Matcher anyWordMatcher = anyWordPattern.matcher(text).region(startIndex, endIndex);
        Matcher vowelMatcher = vowelPattern.matcher(text).region(startIndex, endIndex);

        String firstVowel = null;
        int firstVowelStartIndex = -1;
        int firstVowelEndIndex = -1;
        if (vowelMatcher.find()) {
            firstVowelStartIndex = vowelMatcher.start();
            firstVowelEndIndex = vowelMatcher.end();
            firstVowel = vowelMatcher.group();
        }

        String longestWord = null;
        int longestWordStartIndex = -1;
        int longestWordEndIndex = -1;
        while (anyWordMatcher.find()) {
            String curWord = anyWordMatcher.group();
            int curWordStartIndex = anyWordMatcher.start();
            int curWordEndIndex = anyWordMatcher.end();

            if (curWordEndIndex >= endIndex) break;
            if (longestWord == null || curWord.length() > longestWord.length()) {
                longestWord = curWord;
                longestWordStartIndex = curWordStartIndex;
                longestWordEndIndex = curWordEndIndex;
            }
        }

        if (debugMode) {
            System.out.printf("Before: %s%n", sentenceBefore);
        }

        boolean needSwap = firstVowel != null && longestWord != null && !firstVowel.equalsIgnoreCase(longestWord);
        if (needSwap) {
            // first swap can mess up our indices (only when first vowel is later than longest word),
            // so we have to adjust
            if (firstVowelStartIndex > longestWordStartIndex) {
                int lengthShift = longestWord.length() - firstVowel.length();
                firstVowelStartIndex -= lengthShift;
                firstVowelEndIndex -= lengthShift;
            }

            text.replace(longestWordStartIndex, longestWordEndIndex, firstVowel);
            text.replace(firstVowelStartIndex, firstVowelEndIndex, longestWord);

            if (debugMode) {
                System.out.printf("SWAPPED: \"%s\" x \"%s\"%n", firstVowel, longestWord);
                String sentenceAfter = text.substring(startIndex, endIndex);
                System.out.printf("After:  %s%n%n", sentenceAfter);
            }
        } else {
            if (debugMode) {
                System.out.printf("NO SWAP%n%n");
            }
        }
    }

    public void processText(StringBuffer text) throws DataFormatException {
        Pattern endOfSentencePattern = Pattern.compile("[\\.\\!\\?]\\s*");

        Matcher sentenceEndMatcher = endOfSentencePattern.matcher(text);

        int sentenceStart = -1;
        int sentenceEnd = -1;
        while(true)
        {
            sentenceStart = (sentenceEnd == -1) ? 0 : (sentenceEndMatcher.end());
            sentenceEnd = (sentenceEndMatcher.find()) ? (sentenceEndMatcher.start() + 1) : text.length();
            processSentence(text, sentenceStart, sentenceEnd);
            if (sentenceEnd == text.length()) break;
        }
    }
}
