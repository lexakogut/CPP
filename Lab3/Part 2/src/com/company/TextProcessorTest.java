package com.company;

import org.junit.jupiter.api.BeforeEach;

import java.util.zip.DataFormatException;

class TextProcessorTest {
    TextProcessor processor;

    @BeforeEach
    void setUp() {
        processor = new TextProcessor(false);
    }

    @org.junit.jupiter.api.Test
    void processTextSimple() throws DataFormatException {
        StringBuffer buffer = new StringBuffer("My name is Oleksiy.");
        processor.processText(buffer);
        assert(buffer.toString().equals("My name Oleksiy is."));
    }

    @org.junit.jupiter.api.Test
    void processTextNoSwap() throws DataFormatException {
        StringBuffer buffer = new StringBuffer("Hello world!");
        processor.processText(buffer);
        assert(buffer.toString().equals("Hello world!"));
    }

    @org.junit.jupiter.api.Test
    void processTextWithComma() throws DataFormatException {
        StringBuffer buffer = new StringBuffer("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        processor.processText(buffer);
        assert(buffer.toString().equals("Lorem consectetur dolor sit amet, ipsum adipiscing elit."));
    }

    @org.junit.jupiter.api.Test
    void processTextNoDotInTheEnd() throws DataFormatException {
        StringBuffer buffer = new StringBuffer("Oh, hello there, General Kenobi");
        processor.processText(buffer);
        assert(buffer.toString().equals("General, hello there, Oh Kenobi"));
    }

    @org.junit.jupiter.api.Test
    void processTextMultipleSentences() throws DataFormatException {
        StringBuffer buffer = new StringBuffer(
                "Lorem ipsum dolor sit amet! " +
                "Consectetur adipiscing elit. " +
                "Sed do eiusmod tempor incididunt ut labore?");
        processor.processText(buffer);
        assert(buffer.toString().equals(
                "ipsum Lorem dolor sit amet! " +
                "adipiscing Consectetur elit. " +
                "Sed do incididunt tempor eiusmod ut labore?"));
    }
}