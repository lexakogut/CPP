package com.company;

import java.util.Random;

public class GraphGenerator {
    private Random random;

    public GraphGenerator() {
        random = new Random();
    }

    public Graph Generate(int nodes, float density, int minWeight, int maxWeight)
    {
        Graph result = new Graph();
        for (int y = 0; y < nodes; ++y) {
            String name = "N" + y;
            result.addNode(name);
        }

        for (int y = 0; y < nodes; ++y) {
            for (int x = y + 1; x < nodes; ++x) {
                float value = random.nextFloat();
                if (value <= density) {
                    int weight = minWeight + random.nextInt(maxWeight - minWeight);
                    result.addPath(y, x, weight);
                }
            }
        }
        return result;
    }
}
