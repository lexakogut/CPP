package com.company;

import sun.plugin2.message.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyWindow extends JFrame {
    final int threadCount = 4;
    Thread[] threads;
    GraphGenerator generator;

    JButton Button_Start;
    JButton Button_Stop;
    JLabel[] Label_Thread;
    Timer timer;

    public MyWindow(String title)
    {
        super(title);
        threads = new Thread[threadCount];
        generator = new GraphGenerator();

        JFrame.setDefaultLookAndFeelDecorated(true);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setPreferredSize(new Dimension(550, 200));

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(4, 2));
        Button_Start = new JButton("Start");
        Button_Start.addActionListener(new Button_Start_Listener());
        panel.add(Button_Start);
        Button_Stop = new JButton("Stop");
        Button_Stop.addActionListener(new Button_Stop_Listener());
        panel.add(Button_Stop);
        Label_Thread = new JLabel[threadCount];
        for (int i = 0; i < threadCount; ++i)
        {
            threads[i] = new MyThread(i);
            Label_Thread[i] = new JLabel("Thread" + i);
            panel.add(Label_Thread[i]);
        }
        super.setContentPane(panel);
        super.pack();

        timer = new Timer(500, new Timer_Listener());
        timer.setRepeats(true);
        timer.start();
    }

    void updateLabels()
    {
        for (int i = 0; i < threadCount; ++i)
        {
            Thread.State state = threads[i].getState();
            int priority = threads[i].getPriority();
            Label_Thread[i].setText(
                    "[Thread " + i + "]" +
                    " State: " + state.toString() +
                    " Priority: " + priority);
        }
    }

    class Timer_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            updateLabels();
        }
    }

    class Button_Start_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Button Start!");

            for (int i = 0; i < threadCount; ++i)
            {
                threads[i].start();
            }
        }
    }

    class Button_Stop_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Button Stop!");

            for (int i = 0; i < threadCount; ++i)
            {
                //threads[i].interrupt();
                threads[i].stop();
            }
        }
    }

    class MyThread extends Thread {
        int id;

        @Override
        public void run() {
            System.out.println("Thread" + id + " Start!");
            Graph graph = generator.Generate(600, 0.5f, 3, 20);
            System.out.println("Thread" + id + " Graph generated. Searching for a tree...");
            PrimMinimalTreeSearch search = new PrimMinimalTreeSearch();
            Graph result = search.SearchMST(graph);
            System.out.println("Thread" + id + " Found the smallest tree!");
            System.out.println("Thread" + id + " Stoped");
        }

        public MyThread(int id) {
            super();
            this.id = id;
        }
    }
}
