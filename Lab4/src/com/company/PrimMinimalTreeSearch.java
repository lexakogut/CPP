package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PrimMinimalTreeSearch {
    public Graph SearchMST(Graph graph) {
        Graph result = new Graph();
        for (Graph.Node node : graph.nodes) {
            try {
                result.addNode(node.Name);
            }
            catch (Exception e) {

            }
        }

        List<Integer> visitedNodes = new ArrayList<>();
        List<Graph.Path> availablePaths = new ArrayList<>();

        Graph.Node curNode = graph.nodes.get(0);
        visitedNodes.add(curNode.Id);

        while (visitedNodes.size() < result.nodes.size()){
            availablePaths.addAll(curNode.paths.values());
            availablePaths.removeIf(new Predicate<Graph.Path>() {
                @Override
                public boolean test(Graph.Path path) {
                    return visitedNodes.contains(path.DestinationIndex);
                }
            });

            int bestDistance = 999999;
            Graph.Path bestPath = null;
            for (Graph.Path path : availablePaths)
            {
                if (path.Weight < bestDistance)
                {
                    bestDistance = path.Weight;
                    bestPath = path;
                }
            }

            result.addPath(bestPath.SourceIndex, bestPath.DestinationIndex, bestDistance);
            curNode = graph.nodes.get(bestPath.DestinationIndex);
            visitedNodes.add(curNode.Id);
        }

        return result;
    }
}
